import pysimplenetwork

c = pysimplenetwork.SimpleUDPSock(host='127.0.0.1', port=8080)
print("Connection acquired on 127.0.0.1:8080")

buffer = []
codes = {
    ":gm": """self._levelmgr.entity_manager.add(self._player, components.Health(10e200))
print(self._levelmgr.entity_manager.get(self._player, components.Health).max_hp)
self._levelmgr.entity_manager.get(self._player, components.Score).value = 10e40
""",
    ":nh": "self._levelmgr.entity_manager.removeComponent(self._player, components.Hitbox)"
}


def send(code):
    c.send(code.encode())
    data, _ = c.recv()
    print(data.decode())


while True:
    cmd = input("$ " if not buffer else "_ ")

    if cmd == "help":
        print("help (print the help) - quit (quit the program) - run (execute the code in the buffer) - reset (empty the buffer) - ", end='')
        print(":gm (god mode) - :nh (remove player hitbox)")
    elif cmd == "quit":
        break
    elif cmd == "run":
        send("\n".join(buffer))
        buffer = []
    elif cmd == "reset":
        buffer = []
    elif cmd in codes:
        buffer = []
        send(codes[cmd])
    else:
        buffer.append(cmd)

c.close()