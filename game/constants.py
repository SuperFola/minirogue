# window size in pixels
WIDTH = 600
HEIGHT = 600

# framerate limit
FPS_LIMIT = 120

DEBUG = False

# entities speeds
# player
SPEED = 1999  # pixels/second
# ennemies
ENNEMIES_SPEED = 150
# ranged weapons
AMMO_SPEED = 400
AMMO_SPEED_FIREBALL = 300
AMMO_SPEED_PLASMA = 400

# damages
DAMAGES = 10
DMG_PLASMA_COEFF = 2
DMG_SWORD_COEFF = 1.1

# distance between position of the boxes in inventory
D_POS = 60
# number of boxes available in inventory
NB_BOXES = 25
