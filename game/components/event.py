from .. import ecs
from ..utils.Vec2D import Vec2D
import pygame


# TODO: components_collection must also tell on which entity we should add the component

class KeyEvent(ecs.Component):
    def __init__(self, key, keyUp: bool):
        self.key = key
        self.keyUp = keyUp


class MouseClickEvent(ecs.Component):
    def __init__(self, pos: Vec2D, buttonUp: bool):
        self.pos = pos
        self.buttonUp = buttonUp


class OnKeyEvent(ecs.Component):
    def __init__(self, key, keyUp : bool, components_collection, permanent=True):
        self.key = key
        self.keyUp = keyUp
        self.components_collection = components_collection
        self.permanent = permanent


class OnMouseClickEvent(ecs.Component):
        def __init__(self, area: pygame.Rect, buttonUp: bool, components_collection, permanent=True):
            self.area = area
            self.buttonUp = buttonUp
            self.components_collection = components_collection
            self.permanent = permanent
