from .. import ecs
from enum import Enum


class EntityType(Enum):
    Ammo = 0
    ContactDamage = 1
    Player = 2
    Ennemy = 3
    Object = 4


class Type(ecs.Component):
    def __init__(self, etype: EntityType, permanent=False):
        self.type = etype
        self.permanent = permanent