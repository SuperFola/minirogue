from .. import ecs
import pygame
from ..utils.Vec2D import Vec2D


# the hitbox is moved automatically by the movement system,
# to stay on the entity it is assigned to, if the entity has
# a position component

class Hitbox(ecs.Component):
    def __init__(self, size: tuple, on_collide: object=None):
        if len(size) == 2:
            self.rectangle = pygame.Rect(0, 0, *size)
        else:
            assert(len(size) == 4)
            self.rectangle = pygame.Rect(*size)
        self.collide_groups = None
        # on_collide should be a function taking 3 arguments:
        #   the entity manager, the entity on which it's bound,
        #   and the entity colliding with it
        self.on_collide = on_collide
    
    @property
    def center(self):
        return Vec2D(self.rectangle.x + self.rectangle.w / 2, self.rectangle.y + self.rectangle.h / 2)
    
    @center.setter
    def center(self, value: Vec2D):
        self.rectangle.x = value.x - self.rectangle.w / 2
        self.rectangle.y = value.y - self.rectangle.h / 2
    
    def move(self, vec: Vec2D):
        self.rectangle.x += vec.x
        self.rectangle.y += vec.y