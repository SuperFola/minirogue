from .. import ecs


class Callback(ecs.Component):
    def __init__(self, f: object):
        self._callback = f
    
    def __call__(self, dt: float):
        self._callback(dt)