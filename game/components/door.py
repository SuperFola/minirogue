from .. import ecs


class Door(ecs.Component):
    def __init__(self, dest: tuple, callback: object, active: bool):
        self.dest = dest
        self.callback = callback
        self.active = active