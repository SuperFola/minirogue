from .. import ecs


class Timer(ecs.Component):
    def __init__(self, time: float, components: list, repeat=False):
        self.max_time = time
        self.time = time
        self.components = components
        self.repeat = repeat