from .. import ecs
from ..utils.Vec2D import Vec2D


class Movement(ecs.Component):
    def __init__(self, velocity: Vec2D):
        self.vel = velocity