from .. import ecs


class Health(ecs.Component):
    def __init__(self, max_hp):
        self.hp = max_hp
        self.max_hp = max_hp
        self.last_modifier = None  # ecs.Entity, the last entity to have modified the health