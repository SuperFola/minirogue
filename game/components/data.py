from .. import ecs


class Data(ecs.Component):
    def __init__(self, **kwargs):
        self._data = dict(**kwargs)
    
    def __getitem__(self, index):
        return self._data[index]
    
    def __setitem__(self, index, value):
        self._data[index] = value
    
    def has(self, index):
        return self._data.get(index, None) != None