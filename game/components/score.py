from .. import ecs


class Score(ecs.Component):
    def __init__(self):
        self.value = 0
        self.hold_value = 0