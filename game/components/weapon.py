from .. import ecs
from enum import Enum
from ..constants import DAMAGES


class WeaponsHolder(ecs.Component):
    def __init__(self, owner: ecs.Entity, *weapons, move_callback: object=None):
        self.owner = owner
        self.weapons = list(weapons)
        self.base_damages = DAMAGES
        self.move_callback = move_callback


class WeaponType(Enum):
    Fireball = 0
    Plasma = 1
    Sword = 2


class Weapon(ecs.Component):
    def __init__(self, cooldown, wtype: WeaponType, ranged: bool, active=False):
        self.max_cooldown = cooldown
        self.cooldown = cooldown
        self.type = wtype
        self.ranged = ranged
        self.active = active