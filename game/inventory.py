import pygame
from . import systems
from . import components
from . import ecs
from . import constants
from .constants import *
from .utils.Vec2D import Vec2D
from .utils.reader import Reader


class Inventory:
    def __init__(self, screen):
        self.screen = screen
        self._systems = []
        self.running = True
        self._clock = pygame.time.Clock()
        self.font = pygame.font.Font(None, 50)  # load default font of pygame
        self._entity_manager = ecs.EntityManager()
        self._inventLayer = components.Layer(z=0)
        self._pygame_event = ecs.Entity()
        self._boxSurface = pygame.Surface((50, 50))
        self._boxes = []
        self.playerBag=None
        self.e=[]
        self._prepare()

    def dispatchEvent(self, event):
        if event.type == pygame.QUIT:
            self.running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            self._entity_manager.add(self._pygame_event, components.MouseClickEvent(Vec2D(*event.pos), False))
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_e:
                self._close()
    
    def update(self, dt: float):
        for system in self._systems:
            system.update(dt)
    
    def _close(self):
        self.running = False

    def _prepare(self):
        self.running = True

        # Create the list of positions of boxes in the inventory
        for i in range(NB_BOXES):
            self._boxes.append((5 + D_POS * (i % (WIDTH // D_POS)), 5 + D_POS * (i // (WIDTH // D_POS))))

        for stype in (systems.DestructionSystem, systems.RenderingSystem, systems.EventSystem, systems.CallbackSystem):
            self._systems.append(stype(self._entity_manager, self.screen))
        
        # create entities that are bound to the boxes
        for i, pos in enumerate(self._boxes):
            self.e.append(ecs.Entity())
            f = lambda dt, i=i: self.openLogbook(i)
            self._entity_manager.add(self.e[i], components.Layer(-1)) \
                .add(self.e[i], components.Renderable(self._boxSurface)) \
                .add(self.e[i], components.Position(*pos)) \
                .add(self.e[i], components.OnMouseClickEvent(pygame.Rect(*pos, 50, 50), False, [
                    components.Callback(f)
                ], permanent=True)) \
                .add(self.e[i], components.Data(index=i))
            self._boxSurface.fill((50, 50, 50))
    
    def openLogbook(self, i):
        if 0 <= i < len(self.playerBag.content):
            with open(self.playerBag.content[i]) as file:
                content = file.read()
            
            reader = Reader(content, pos=(20, 20), width=WIDTH - 40, fontsize=16, height=HEIGHT - 40,
                bg=(150, 150, 150), fgcolor=(20, 20, 20))
            reader.show()
            while True:
                ev = pygame.event.poll()
                reader.update(ev)

                if ev.type == pygame.QUIT:
                    break
                elif ev.type == pygame.KEYDOWN:
                    if ev.key == pygame.K_ESCAPE:
                        break
                reader.show()
    
    def run(self, bag):
        self.running = True
        self.playerBag = bag

        for j in self.e:
            idx = self._entity_manager.get(j, components.Data)["index"]
            if idx < len(self.playerBag.content):
                self._entity_manager.add(j, components.Renderable([
                    "game/assets/paper/0.png",
                    "game/assets/paper/1.png",
                    "game/assets/paper/2.png"
                ]))
        
        while self.running:
            # dt is in milliseconds
            dt = self._clock.tick(FPS_LIMIT)
            
            if DEBUG:
                pygame.display.set_caption(f"MiniPolyRogue - FPS: {1.0 / dt * 1000:.1f}")

            self.dispatchEvent(pygame.event.poll())

            # the delta time given to the update method must be in seconds
            self.update(dt / 1000)
            pygame.display.flip()
