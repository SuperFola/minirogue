from .. import ecs
from ..components.health import Health
from ..components.destroyed import Destroyed
from ..components.score import Score
from ..constants import DEBUG


class HealthSystem(ecs.System):
    def __init__(self, entity_manager, *args):
        super().__init__(entity_manager, Health)

    def update(self, _):
        for e, health in self:
            if health.hp <= 0:
                if DEBUG:
                    print(f"healthSystem: {e} was destroyed by {health.last_modifier}")
                self.entity_manager.add(e, Destroyed())

                score = self.entity_manager.get(health.last_modifier, Score)
                # increase score by `health.max_hp` points
                if score:
                    score.value += health.max_hp
            
            if health.hp > health.max_hp:
                health.hp = health.max_hp