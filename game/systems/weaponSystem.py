from .. import ecs
import pygame
from ..components.projectile import Projectile
from ..components.movement import Movement
from ..components.renderable import Renderable
from ..components.layer import Layer
from ..components.hitbox import Hitbox
from ..components.position import Position
from ..components.weapon import Weapon, WeaponsHolder, WeaponType
from ..components.types import Type, EntityType
from ..components.damage import Damage
from ..components.health import Health
from ..components.destroyed import Destroyed
from ..components.data import Data
from ..components.callback import Callback
from ..components.timer import Timer
from ..constants import *
from ..utils.Vec2D import Vec2D


# handling Ammo
def ammo_collide(em, e, o):
    otype = em.get(o, Type)
    # invert velocity if the ammo get hits by a "contact damage"
    if otype and otype.type == EntityType.ContactDamage:
        em.get(e, Movement).vel.rand_inv()
    elif otype and otype.type == EntityType.Object:
        if not em.get(e, Type).permanent:
            # destroy entity
            em.add(e, Destroyed())
        else:
            em.get(e, Movement).vel.rand_inv()

    ohealth = em.get(o, Health)
    # add damages
    if ohealth:
        damage = em.get(e, Damage)
        # avoid taking damage from ourselves
        if damage is None or damage.from_ == o:
            return
        ohealth.hp -= damage.quantity
        ohealth.last_modifier = damage.from_

        if not em.get(e, Type).permanent:
            # destroy entity
            em.add(e, Destroyed())


# handling ContactDamage
def cd_collide(em, e, o):
    ohealth = em.get(o, Health)
    # add damages
    if ohealth:
        damage = em.get(e, Damage)
        # avoid taking damage from ourselves
        if damage is None or damage.from_ == o:
            return
        ohealth.hp -= damage.quantity
        ohealth.last_modifier = damage.from_

    if not em.get(e, Type).permanent:
        # destroy entity
        em.add(e, Destroyed())


class WeaponSystem(ecs.System):
    def __init__(self, entity_manager, *args):
        super().__init__(entity_manager, WeaponsHolder, Projectile)

    def update(self, dt):
        to_remove = []
        to_generate = []

        # update cooldowns
        for entity, wholder in self.entity_manager.iter(WeaponsHolder):
            for weapon in wholder.weapons:
                weapon.cooldown -= dt
                
        for entity, (wholder, projectile) in self:
            # remove the projectile
            if not projectile.permanent:
                to_remove.append(entity)

            for weapon in wholder.weapons:
                # only launch the projectile/attack if the weapon is active,
                # and if the cooldown is <= 0.0
                if weapon.active:
                    if weapon.cooldown > 0.0:
                        continue
                    
                    projectile = self.entity_manager.get(wholder.owner, Projectile)
                    if projectile is None:
                        continue
                    
                    o = ecs.Entity()
                    # move a little bit the projectile to avoid putting it immediatly on its summoner
                    # and thus applying damages to them
                    x = projectile.origin.x + projectile.vec_dir.x * projectile.origin_hitbox.w * 1.5
                    y = projectile.origin.y + projectile.vec_dir.y * projectile.origin_hitbox.h * 1.5

                    # handle ranged weapons separatly from the contact weapons
                    if weapon.ranged:
                        if weapon.type == WeaponType.Fireball:
                            to_generate.append({
                                "position": Position(x, y),
                                "movement": Movement(projectile.vec_dir * AMMO_SPEED_FIREBALL),
                                "renderable": Renderable([
                                    "game/assets/fireball/0.png",
                                    "game/assets/fireball/1.png",
                                    "game/assets/fireball/2.png"
                                ]),
                                "layer": Layer(z=1),
                                "hitbox": Hitbox(
                                    (32, 32),
                                    on_collide=ammo_collide
                                ),
                                "entity": o,
                                "type": Type(EntityType.Ammo),
                                "damage": Damage(wholder.base_damages, wholder.owner),
                                "data": {"type": WeaponType.Fireball},
                                "move": wholder.move_callback(o) if wholder.move_callback else None,
                                "timer": Timer(1.5, [Destroyed()])
                            })
                        elif weapon.type == WeaponType.Plasma:
                            to_generate.append({
                                "position": Position(x, y),
                                "movement": Movement(projectile.vec_dir * AMMO_SPEED_PLASMA),
                                "renderable": Renderable("game/assets/plasma.png"),
                                "layer": Layer(z=1),
                                "hitbox": Hitbox(
                                    (32, 32),
                                    on_collide=ammo_collide
                                ),
                                "entity": o,
                                "type": Type(EntityType.Ammo),
                                "damage": Damage(DMG_PLASMA_COEFF * wholder.base_damages, wholder.owner),
                                "data": {"type": WeaponType.Plasma},
                                "move": wholder.move_callback(o) if wholder.move_callback else None,
                                "timer": Timer(1.5, [Destroyed()])
                            })
                    else:
                        to_generate.append({
                            "position": Position(x, y),
                            "movement": Movement(Vec2D(0, 0)),
                            "renderable": None,
                            "layer": None,
                            "hitbox": Hitbox(
                                (24, 24),
                                on_collide=cd_collide
                            ),
                            "entity": o,
                            "type": Type(EntityType.ContactDamage),
                            "damage": Damage(DMG_SWORD_COEFF * wholder.base_damages, wholder.owner),
                            "data": {"type": WeaponType.Sword},
                            "move": None,
                            "timer": Timer(0.2, [Destroyed()])
                        })
                    
                    # reset cooldown
                    weapon.cooldown = weapon.max_cooldown
        
        # generate the ammo
        for data in to_generate:
            pos = data["position"]
            pos.x -= data["hitbox"].rectangle.w // 2
            pos.y -= data["hitbox"].rectangle.h // 2
            self.entity_manager.add(data["entity"], pos)
            self.entity_manager.add(data["entity"], data["movement"])
            if data["renderable"]:
                self.entity_manager.add(data["entity"], data["renderable"])
            if data["layer"]:
                self.entity_manager.add(data["entity"], data["layer"])
            self.entity_manager.add(data["entity"], data["hitbox"])
            self.entity_manager.add(data["entity"], data["type"])
            if data["damage"]:
                self.entity_manager.add(data["entity"], data["damage"])
            self.entity_manager.add(data["entity"], Data(**data["data"]))
            if data["timer"]:
                self.entity_manager.add(data["entity"], data["timer"])
            if data["move"]:
                self.entity_manager.add(data["entity"], Timer(4 / FPS_LIMIT, [Callback(data["move"])], repeat=True))
        
        # remove components
        for entity in to_remove:
            self.entity_manager.removeComponent(entity, Projectile)