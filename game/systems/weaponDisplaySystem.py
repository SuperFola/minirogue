from .. import ecs
import pygame
from ..components.position import Position
from ..components.renderable import Renderable
from ..components.weapon import Weapon, WeaponsHolder, WeaponType
from ..components.types import Type, EntityType
from ..constants import *
from ..utils.Vec2D import Vec2D


class WeaponDisplaySystem(ecs.System):
    def __init__(self, entity_manager, screen, *args):
        super().__init__(entity_manager, WeaponsHolder, Type)
        self.screen = screen

        # [0] is left, [1] is right
        self._images = {
            WeaponType.Fireball: [None, None],
            WeaponType.Plasma: [None, None],
            WeaponType.Sword: [None, None]
        }

        # store rotated images
        self._images[WeaponType.Fireball][0] = pygame.transform.flip(pygame.image.load("game/assets/fire_gun.png").convert_alpha(), True, False)
        self._images[WeaponType.Fireball][1] = pygame.image.load("game/assets/fire_gun.png").convert_alpha()

        self._images[WeaponType.Plasma][0] = pygame.transform.flip(pygame.image.load("game/assets/plasma_gun.png").convert_alpha(), True, False)
        self._images[WeaponType.Plasma][1] = pygame.image.load("game/assets/plasma_gun.png").convert_alpha()

        self._images[WeaponType.Sword][0] = pygame.transform.flip(pygame.image.load("game/assets/sword.png").convert_alpha(), True, False)
        self._images[WeaponType.Sword][1] = pygame.image.load("game/assets/sword.png").convert_alpha()

    def update(self, dt):
        for e, (wholder, type_) in self:
            # only display player weapons
            if type_.type == EntityType.Player:
                r = self.entity_manager.get(e, Renderable)
                right = not r.flip_x
                p = self.entity_manager.get(e, Position)

                # retrieve the active weapon
                weapon_active = None
                for w in wholder.weapons:
                    if w.active:
                        weapon_active = w
                        break
                
                # display it if we found it
                if weapon_active is not None:
                    img = self._images[weapon_active.type][1 if right else 0]
                    self.screen.blit(img, (
                        p.x + r.width if right else p.x - img.get_width(),
                        p.y + 12
                    ))