from .. import ecs
from ..components.event import KeyEvent, MouseClickEvent, OnKeyEvent, OnMouseClickEvent
from ..utils.Vec2D import Vec2D


# if there is a callback attached to an entity, execute it
# then remove the callback component

class EventSystem(ecs.System):
    def __init__(self, entity_manager, screen, *args):
        super().__init__(entity_manager)
        self._screen = screen

    def update(self, dt):
        to_remove_keys, to_remove_mouseclick = [], []
        to_remove_onkeys, to_remove_onmouseclick = [], []

        for e, keyevent in self.entity_manager.iter(KeyEvent):
            if e not in to_remove_keys:
                to_remove_keys.append(e)
            
            for o, onkeyevent in self.entity_manager.iter(OnKeyEvent):
                if onkeyevent.key == keyevent.key and onkeyevent.keyUp == keyevent.keyUp:
                    if not onkeyevent.permanent:
                        to_remove_onkeys.append(o)
                    
                    for c in onkeyevent.components_collection:
                        self.entity_manager.add(o, c)
        
        for e, mouseclickevent in self.entity_manager.iter(MouseClickEvent):
            if e not in to_remove_mouseclick:
                to_remove_mouseclick.append(e)

            for o, onmouseclickevent in self.entity_manager.iter(OnMouseClickEvent):
                if onmouseclickevent.area.collidepoint(mouseclickevent.pos.tuple) != 0 and mouseclickevent.buttonUp == onmouseclickevent.buttonUp:
                    if not onmouseclickevent.permanent:
                        to_remove_onmouseclick.append(o)
                    
                    for c in onmouseclickevent.components_collection:
                        self.entity_manager.add(o, c)
        
        # remove components
        for e in to_remove_keys:
            self.entity_manager.removeComponent(e, KeyEvent)
        for e in to_remove_mouseclick:
            self.entity_manager.removeComponent(e, MouseClickEvent)
        for e in to_remove_onkeys:
            self.entity_manager.removeComponent(e, OnKeyEvent)
        for e in to_remove_onmouseclick:
            self.entity_manager.removeComponent(e, OnMouseClickEvent)