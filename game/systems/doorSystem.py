from .. import ecs
from ..components.door import Door
from ..components.types import Type, EntityType
from ..components.position import Position
from ..components.renderable import Renderable
from ..components.hitbox import Hitbox
from ..components.layer import Layer
from ..components.bag import Bag
import pygame
from ..constants import WIDTH, HEIGHT, DEBUG


def chest_collide(treasure):
    def wrapper(em, e, o):
        if not hasattr(wrapper, "gave"):
            wrapper.gave = False
        
        otype = em.get(o, Type)
        if not wrapper.gave and otype and otype.type == EntityType.Player:
            wrapper.gave = True
            if DEBUG:
                print("Got treasure", treasure)
            em.get(o, Bag).content.append(treasure)
            em.add(e, Renderable("game/assets/chest/opened.png"))
            em.removeComponent(e, Hitbox)
    return wrapper


FILES = 8


# doors must be activated only if we killed all the ennemies in a room

class DoorSystem(ecs.System):
    def __init__(self, entity_manager, *args):
        super().__init__(entity_manager, Door)
        self._has_ennemies = False
        self._has_put_chest = False
    
    def update(self, _):
        global FILES

        for e, type_ in self.entity_manager.iter(Type):
            if type_.type == EntityType.Ennemy:
                # quit the function to avoid activating the doors
                self._has_ennemies = True
                return

        for e, door in self:
            if door.active:
                return
            
            door.active = True
            # rotate the door
            p, r = self.entity_manager.get(e, Position), self.entity_manager.get(e, Renderable)
            r.sprite = pygame.image.load("game/assets/door/opened.png").convert_alpha()
            r.rotate(r.angle)
        
        # spawning a chest when all ennemies are dead
        # TODO improve, hard coding is bad
        if FILES and self._has_ennemies and not self._has_put_chest:
            self._has_put_chest = True
            chest = ecs.Entity()
            FILES -= 1

            self.entity_manager.add(chest, Position(WIDTH // 2 - 30, HEIGHT // 2 - 30)) \
                .add(chest, Hitbox((60, 60), on_collide=chest_collide(f"game/assets/logbook/{7 - FILES}.txt"))) \
                .add(chest, Type(EntityType.Object)) \
                .add(chest, Layer(3)) \
                .add(chest, Renderable("game/assets/chest/closed.png"))