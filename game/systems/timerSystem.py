from .. import ecs
from ..components.timer import Timer
from ..components.destroyed import Destroyed


class TimerSystem(ecs.System):
    def __init__(self, entity_manager, *args):
        super().__init__(entity_manager, Timer)

    def update(self, dt):
        to_remove = []

        for entity, timer in self:
            timer.time -= dt
            if timer.time <= 0.0:
                if timer.repeat:
                    timer.time = timer.max_time
                else:
                    to_remove.append(entity)
                
                # add the given components
                for c in timer.components:
                    self.entity_manager.add(entity, c)
        
        for entity in to_remove:
            self.entity_manager.removeComponent(entity, Timer)