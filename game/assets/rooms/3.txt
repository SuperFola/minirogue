[
    [
        components.Position(428, 468),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(468, 428),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    
    [
        components.Position(468, 468),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(172, 468),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(132, 428),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(132, 468),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(468, 172),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(428, 132),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(468, 132),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(172, 132),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(132, 172),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(132, 132),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(0, 0),
        components.Renderable("game/assets/background.png"),
        components.Layer(z=-2)
    ],
    [
        components.Position(0, 0),
        components.Renderable("game/assets/walls.png"),
        components.Layer(z=-1)
    ],
    [
        components.Position(0, 0),
        components.Hitbox((32, HEIGHT)),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(0, 0),
        components.Hitbox((WIDTH, 32)),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(0, HEIGHT - 32),
        components.Hitbox((WIDTH, 32)),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(WIDTH - 32, 0),
        components.Hitbox((32, HEIGHT)),
        components.Type(components.EntityType.Object)
    ]
]
