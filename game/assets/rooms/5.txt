[
    
    
    [
        components.Position(360, 280),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
   
    [
        components.Position(200, 280),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    
    [
        components.Position(280, 360),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    
    [
        components.Position(280, 200),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
     [
        components.Position(400, 280),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
   
    [
        components.Position(160, 280),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    
    [
        components.Position(280, 400),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    
    [
        components.Position(280, 160),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],

    [
        components.Position(440, 280),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
   
    [
        components.Position(120, 280),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    
    [
        components.Position(280, 440),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    
    [
        components.Position(280, 120),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],

    [
        components.Position(360, 360),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],

    [
        components.Position(360, 400),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],

    [
        components.Position(400, 360),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],

    [
        components.Position(200, 200),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],

    [
        components.Position(160, 200),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(200, 160),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],

    [
        components.Position(200, 360),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(160, 360),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(200, 400),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],

    [
        components.Position(360, 200),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(400, 200),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],
    [
        components.Position(360, 160),
        components.Renderable("game/assets/trash.png"),
        components.Hitbox((40, 40)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object),
        components.Health(10)
    ],











    [
        components.Position(0, 0),
        components.Renderable("game/assets/background.png"),
        components.Layer(z=-2)
    ],
    [
        components.Position(0, 0),
        components.Renderable("game/assets/walls.png"),
        components.Layer(z=-1)
    ],
    [
        components.Position(0, 0),
        components.Hitbox((32, HEIGHT)),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(0, 0),
        components.Hitbox((WIDTH, 32)),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(0, HEIGHT - 32),
        components.Hitbox((WIDTH, 32)),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(WIDTH - 32, 0),
        components.Hitbox((32, HEIGHT)),
        components.Type(components.EntityType.Object)
    ],
]

