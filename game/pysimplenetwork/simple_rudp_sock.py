import socket
import hashlib
import struct
from .simple_udp_sock import SimpleUDPSock
from .simple_sock import MAX_PACK_SIZE


class RUDPCorruptedPacketLength(Exception):
    def __init__(self, *args, length=0):
        super().__init__(*args)

        self.packet_length = length


class RUDPCorruptedPacketContent(Exception):
    def __init__(self, *args, content=None):
        super().__init__(*args)

        self.packet_content = content


class RUDPPacketWrongOrder(Exception):
    def __init__(self, *args, order=0, expected_order=0):
        super().__init__(*args)

        self.packet_order = order
        self.expected_packet_order = expected_order


class RUDPPack:
    header_size = 4 + 4 + 64

    def __init__(self, data, order=0):
        self.size = len(data)
        self.order = order
        self.checksum = hashlib.sha3_256(f"{self.size}{self.order}{data}".encode('utf-8')).hexdigest().encode('utf-8')
        self.data = data

    def pack(self):
        return struct.pack(f"II64s{self.size}s", self.size, self.order, self.checksum, self.data)

    @staticmethod
    def unpack(data):
        if len(data) < RUDPPack.header_size:
            raise RUDPCorruptedPacketLength(length=len(data))

        size, order, checksum = struct.unpack("II64s", data[:RUDPPack.header_size])
        content, *_ = struct.unpack(f"{size}s", data[RUDPPack.header_size:])

        if checksum != hashlib.sha3_256(f"{size}{order}{content}".encode('utf-8')).hexdigest().encode('utf-8'):
            raise RUDPCorruptedPacketContent(content={
                "size": size,
                "order": order,
                "content": content
            })

        return size, order, content


class SimpleRUDPSock(SimpleUDPSock):
    def __init__(self, host=None, port=8080, bind=False):
        super().__init__(host, port, bind)

        self.__packet_order = 0
        self.__packet_recv = 0

    def _send(self, data, destination):
        self.__packet_order += 1
        self.sock.sendto(data, destination)

    def send(self, msg):
        size = len(msg)

        if size < MAX_PACK_SIZE:
            pack = RUDPPack(msg, self.__packet_order)
            self._send(pack.pack(), (self.host, self.port))
        else:
            while size:
                if size >= MAX_PACK_SIZE - RUDPPack.header_size:
                    pack = RUDPPack(msg[:MAX_PACK_SIZE - RUDPPack.header_size], self.__packet_order)
                    self._send(pack.pack(), (self.host, self.port))
                    msg = msg[MAX_PACK_SIZE - RUDPPack.header_size:]
                else:
                    pack = RUDPPack(msg, self.__packet_order)
                    self._send(pack.pack(), (self.host, self.port))
                    msg = msg[size:]
                size = len(msg)

    def recv(self):
        msg = b""

        while True:
            data, _ = self._recv(MAX_PACK_SIZE)
            size, order, content = RUDPPack.unpack(data)

            if order != self.__packet_recv:
                raise RUDPPacketWrongOrder(order=order, expected_order=self.__packet_recv)

            msg += content

            self.__packet_recv += 1
            if size < MAX_PACK_SIZE - RUDPPack.header_size:
                break

        return msg
