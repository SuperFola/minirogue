from . import *
import random, time, string


failures = 0


def sendall(msgs, client, server):
    global failures

    for i, m in enumerate(msgs):
        encoded = m.encode('utf-8') if isinstance(m, str) else utils.to_bytes(m)
        print(f"[{i}] Sending {len(encoded)} bytes...", end=" ", flush=True)
        client.send(encoded)
        time.sleep(1)

        print("Receiving...", end=" ", flush=True)
        o = server.recv()
        if o == encoded:
            print(f"{len(o)} bytes", flush=True)
        else:
            failures += 1
            print(f"Wrong number of bytes received: {len(o)}", flush=True)
        time.sleep(1)


if __name__ == '__main__':
    msgs = [
        "hello world",
        "".join(random.choice(string.ascii_letters) for _ in range(simple_sock.MAX_PACK_SIZE * 2 + 4)),
        0, 1, 465432
    ]

    print("Testing numbers encoding")
    numbers = [0, 1, 8, 88, 456, 7854, 365778]
    for n in numbers:
        if n != to_int(to_bytes(n)):
            failure += 1

    print("-" * 15)
    print("Testing UDP sockets")
    client_udp = simple_rudp_sock.SimpleUDPSock(host='127.0.0.1', port=45000)
    server_udp = simple_rudp_sock.SimpleUDPSock(host='127.0.0.1', port=45000, bind=True)
    sendall(msgs, client_udp, server_udp)
    client_udp.close()
    server_udp.close()

    print("-" * 15)
    print("Testing RUDP sockets")
    client_rudp = simple_rudp_sock.SimpleRUDPSock(host='127.0.0.1', port=45001)
    server_rudp = simple_rudp_sock.SimpleRUDPSock(host='127.0.0.1', port=45001, bind=True)
    sendall(msgs, client_rudp, server_rudp)
    client_rudp.close()
    server_rudp.close()

    print(f"{failures} test(s) failed")
