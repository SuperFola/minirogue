from random import choice


class Vec2D:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    
    def __mul__(self, z):
        return Vec2D(self.x * z, self.y * z)
    
    def normalize(self):
        n = (self.x ** 2 + self.y ** 2) ** 0.5
        self.x /= n
        self.y /= n
    
    def invert(self):
        self.x *= -1
        self.y *= -1
    
    def rand_inv(self):
        self.x *= choice([-1, 1])
        self.y *= choice([-1, 1])
    
    def reset(self):
        self.x = 0
        self.y = 0
    
    @property
    def tuple(self):
        return (self.x, self.y)
    
    def scalaire(self, other):
        return self.x * other.x + self.y * other.y
    
    def __repr__(self):
        return f"Vec2D(x={self.x}, y={self.y})"
    
    __str__ = __repr__