import pygame
from . import systems
from . import components
from . import ecs
from .constants import *
import math, sys
from .utils.Vec2D import Vec2D
from random import randint
from .levelManager import LevelManager
from .inventory import Inventory
from . import pysimplenetwork
from threading import Thread
from enum import Enum


class Game:
    def __init__(self, screen):
        self.screen = screen
        self._systems = []
        self.running = True
        self._clock = pygame.time.Clock()
        self.font = pygame.font.Font(None, 18)
        self._WeaponNameText = ecs.Entity()

        # summoning the Level Manager
        self._levelmgr = LevelManager(self.screen)
        self._levelmgr.load()
        self._levelmgr.init()  # must be called after levelmgr.load()
        self._player = self._levelmgr.generatePlayer()
        self._levelmgr.generateCurrentRoom(0, self._player)

        self._invent = Inventory(self.screen)
        self._move = Vec2D()
    
    def update(self, dt: float):
        for system in self._levelmgr.systems:
            system.update(dt)
        
        # rotate the player
        x, _ = pygame.mouse.get_pos()
        if not self._levelmgr.entity_manager.hasEntity(self._player):
            return
        x -= self._levelmgr.entity_manager.get(self._player, components.Position).x
        r = self._levelmgr.entity_manager.get(self._player, components.Renderable)
        if x <= 0:
            # rotate to the left
            if not r.flip_x:
                r.flipX()
        else:
            # rotate to the right
            if r.flip_x:
                r.flipX()

    def dispatchEvent(self, event):
        if event.type == pygame.QUIT:
            self.running = False
        
        # handle player movement, separated from the standard systems
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_z:
                self._move.y = -SPEED
            if event.key == pygame.K_s:
                self._move.y = SPEED
            if event.key == pygame.K_q:
                self._move.x = -SPEED
            if event.key == pygame.K_d:
                self._move.x = SPEED
            if event.key == pygame.K_e:
                self._invent.run(self._levelmgr.entity_manager.get(self._player, components.Bag))
            if event.key == pygame.K_SPACE:
                self._move.x = SPEED * 4 * (-1 if self._levelmgr.entity_manager.get(self._player, components.Renderable).flip_x else 1)
        
        # summon projectile
        if event.type == pygame.MOUSEBUTTONUP:
            if event.button == pygame.BUTTON_LEFT or event.button == pygame.BUTTON_RIGHT or event.button == pygame.BUTTON_MIDDLE:
                player_hitbox = self._levelmgr.entity_manager.get(self._player, components.Hitbox)
                mx, my = event.pos
                dx, dy = mx - player_hitbox.center.x, my - player_hitbox.center.y
                n = (dx ** 2 + dy ** 2) ** 0.5
                vec_dir = Vec2D(dx / n, dy / n)
                
                self._levelmgr.entity_manager.add(self._player, components.Projectile(player_hitbox.center, vec_dir, player_hitbox.rectangle))

            # change active weapon
            elif event.button == pygame.BUTTON_WHEELUP:
                for n, i in enumerate(self._levelmgr.entity_manager.get(self._player, components.WeaponsHolder).weapons):
                    if i.active == True:
                        i.active = False
                        self._levelmgr.entity_manager.add(self._WeaponNameText, components.Layer(z=2)) \
                            .add(self._WeaponNameText, components.Position(self._levelmgr.entity_manager.get(self._player,components.Position).x - 10, self._levelmgr.entity_manager.get(self._player,components.Position).y - 25))
                        if n + 1 >= len(self._levelmgr.entity_manager.get(self._player, components.WeaponsHolder).weapons):
                            n = 0
                            self._levelmgr.entity_manager.get(self._player, components.WeaponsHolder).weapons[n].active = True
                            name = str(self._levelmgr.entity_manager.get(self._player,components.WeaponsHolder).weapons[n].type).rsplit(".")
                            self._levelmgr.entity_manager.add(self._WeaponNameText, components.Renderable(self.font.render(str(name[1]), True, pygame.Color('blue'))))
                            break
                        elif n + 1 < len(self._levelmgr.entity_manager.get(self._player, components.WeaponsHolder).weapons):
                            self._levelmgr.entity_manager.get(self._player, components.WeaponsHolder).weapons[n + 1].active = True
                            name = str(self._levelmgr.entity_manager.get(self._player,components.WeaponsHolder).weapons[n + 1].type).rsplit(".")
                            self._levelmgr.entity_manager.add(self._WeaponNameText, components.Renderable(self.font.render(str(name[1]), True, pygame.Color('blue'))))
                            break
                self._levelmgr.entity_manager.add(self._WeaponNameText, components.Timer(2, [components.Destroyed()]))
            elif event.button == pygame.BUTTON_WHEELDOWN:
                for n, i in enumerate(self._levelmgr.entity_manager.get(self._player, components.WeaponsHolder).weapons):
                    if i.active == True:
                        i.active = False
                        self._levelmgr.entity_manager.add(self._WeaponNameText, components.Layer(z=2)) \
                            .add(self._WeaponNameText, components.Position(self._levelmgr.entity_manager.get(self._player,components.Position).x - 10, self._levelmgr.entity_manager.get(self._player,components.Position).y - 25))
                        if n - 1 < 0:
                            n = len(self._levelmgr.entity_manager.get(self._player, components.WeaponsHolder).weapons) - 1
                            self._levelmgr.entity_manager.get(self._player, components.WeaponsHolder).weapons[n].active = True
                            name = str(self._levelmgr.entity_manager.get(self._player,components.WeaponsHolder).weapons[n].type).rsplit(".")
                            self._levelmgr.entity_manager.add(self._WeaponNameText, components.Renderable(self.font.render(str(name[1]), True, pygame.Color('blue'))))
                            break
                        elif n - 1 >= 0:
                            self._levelmgr.entity_manager.get(self._player, components.WeaponsHolder).weapons[n - 1].active = True
                            name = str(self._levelmgr.entity_manager.get(self._player,components.WeaponsHolder).weapons[n - 1].type).rsplit(".")
                            self._levelmgr.entity_manager.add(self._WeaponNameText, components.Renderable(self.font.render(str(name[1]), True, pygame.Color('blue'))))
                            break
                self._levelmgr.entity_manager.add(self._WeaponNameText, components.Timer(2, [components.Destroyed()]))
    
    def debugger(self):
        print("Network debugger started on 127.0.0.1:8080")
        def safe_exec(code):
            nonlocal self
            try:
                old = sys.stdout
                with open("stdout.temp", "w") as sys.stdout:
                    exec(code)
                sys.stdout = old
                with open("stdout.temp") as content:
                    return content.read()
            except Exception as e:
                return str(e)

        server = pysimplenetwork.SimpleUDPSock(host='127.0.0.1', port=8080, bind=True)
        while True:
            data, from_ = server.recv()
            if data:
                server.send(str(safe_exec(data.decode())).encode(), to=from_)
        server.close()

    def run(self):
        # launch network debugger
        if DEBUG:
            debugger = Thread(target=self.debugger)
            debugger.start()

        # launch game
        while self.running:
            # dt is in milliseconds
            dt = self._clock.tick(FPS_LIMIT)
            
            e_count = len(self._levelmgr.entity_manager)
            pygame.display.set_caption(f"MiniPolyRogue - FPS: {1.0 / dt * 1000:.1f} - Entities : {e_count}")

            self.dispatchEvent(pygame.event.poll())
            movement = components.Movement(self._move)
            self._move = Vec2D()
            self._levelmgr.entity_manager.add(self._player, movement)

            # the delta time given to the update method must be in seconds
            self.update(dt / 1000)

            # check if player is dead
            if not self._levelmgr.entity_manager.hasEntity(self._player):
                print("you are dead to me")
                break

            pygame.display.flip()